long long sqrtOfSum(long long a, long long b) {
    int sum = a + b;
    return sum * sum;
}
