#include <iostream>
#include "Helpers.h"
#define PI 3.14
// #define LOG

int main()
{
#ifdef LOG
	std::cout << "log message" << std::endl;
#endif
	int result = sqrtOfSum(12, 24);
	std::cout << "Result = " << result << std::endl;
	return 0;
}
